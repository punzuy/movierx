package mary.movierx.model;

import io.reactivex.Single;
import mary.movierx.model.POJO.MovieResponse;
import mary.movierx.model.POJO.Person;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("person/{person_id}")
    Single<Person> getPersonData(@Path("person_id") int personId,
                                 @Query("api_key") String apiKey);

    @GET("discover/movie")
    Single<MovieResponse> getMovies(@Query("with_people") int personId,
                                    @Query("api_key") String apiKey);

}
