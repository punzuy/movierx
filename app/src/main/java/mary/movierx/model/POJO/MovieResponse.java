package mary.movierx.model.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResponse {

    @SerializedName("results")
    @Expose
    private List<Movie> movies = null;


    /**
     * No args constructor for use in serialization
     */
    public MovieResponse() {
    }

    /**
     * @param movies
     */
    public MovieResponse(Integer page, Integer totalResults, Integer totalPages, List<Movie> movies) {
        super();
        this.movies = movies;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

}
