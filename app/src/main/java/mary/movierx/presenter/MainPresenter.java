package mary.movierx.presenter;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import mary.movierx.model.ApiService;
import mary.movierx.model.NetworkClient;
import mary.movierx.model.POJO.Person;
import mary.movierx.view.MainView;

public class MainPresenter implements IMainPresenter {

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static final String API_KEY = "38722bc1aa187859abfc5ab2b0717374";
    private MainView mainView;

    //тестовый айдишник 16 - не работает, 222 - работает, 287 - Brad Pitt
    private int personId = 287;


    public MainPresenter(MainView mainView) {
        this.mainView = mainView;
        getData();
    }

    private void getData() {
        getSingleObservableWithMovies().subscribe(getObserver());
    }

    private Single<Person> getSingleObservableWithMovies() {
        ApiService apiService = NetworkClient.getRetrofit().create(ApiService.class);

        return apiService.getPersonData(personId, API_KEY)
                .zipWith(apiService.getMovies(personId, API_KEY),
                        (person, movies) -> {
                            person.setMovies(movies.getMovies());
                            return person;
                        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private SingleObserver<Person> getObserver() {
        return new SingleObserver<Person>() {
            @Override
            public void onSubscribe(Disposable d) {
                // we'll come back to this in a moment
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Person person) {
                // data is ready and we can update the UI
                mainView.showResult(person);
            }

            @Override
            public void onError(Throwable e) {
                // oops, we best show some error message
                mainView.showError();
            }
        };
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
