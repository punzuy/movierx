package mary.movierx.view;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import mary.movierx.R;
import mary.movierx.model.POJO.Movie;
import mary.movierx.model.POJO.Person;
import mary.movierx.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements MainView {

    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenter = new MainPresenter(this);
    }

    @Override
    public void showResult(Person person) {
        TextView name = findViewById(R.id.name);
        TextView movie = findViewById(R.id.movie);
        name.setText(person.getName());
        List<Movie> movieList = person.getMovies();
        if (movieList.size() > 0) {
            movie.setText(movieList.get(0).getTitle());
        }
    }

    @Override
    public void showError() {
        TextView name = findViewById(R.id.name);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            name.setText("Error");
        } else {
            name.setText("Error Network");
        }
    }

    @Override
    protected void onDestroy() {
        mainPresenter.onDestroy();
        super.onDestroy();
    }
}
