package mary.movierx.view;

import mary.movierx.model.POJO.Person;

public interface MainView {

    void showResult(Person person);

    void showError();
}
